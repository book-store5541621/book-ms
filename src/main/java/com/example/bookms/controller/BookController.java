package com.example.bookms.controller;

import com.example.bookms.dto.request.BookRequestDto;
import com.example.bookms.dto.response.BookResponseDto;
import com.example.bookms.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @PostMapping
    public void create(@RequestBody BookRequestDto bookRequestDto) {
        bookService.create(bookRequestDto);
    }

    @GetMapping
    public List<BookResponseDto> getAll() {
        return bookService.getAll();
    }

    @GetMapping("/{uuid}")
    public BookResponseDto getById(@PathVariable UUID uuid) {
        return bookService.getById(uuid);
    }
}
