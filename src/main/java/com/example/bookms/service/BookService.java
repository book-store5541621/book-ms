package com.example.bookms.service;

import com.example.bookms.dto.request.BookRequestDto;
import com.example.bookms.dto.response.BookResponseDto;

import java.util.List;
import java.util.UUID;

public interface BookService {
    void create(BookRequestDto book);

    List<BookResponseDto> getAll();

    BookResponseDto getById(UUID uuid);
}
