package com.example.bookms.service.impl;

import com.example.bookms.dto.request.BookRequestDto;
import com.example.bookms.dto.response.BookResponseDto;
import com.example.bookms.model.Book;
import com.example.bookms.repository.BookRepository;
import com.example.bookms.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override
    public void create(BookRequestDto bookRequestDto) {
        bookRepository.save(Book.builder()
                .author(bookRequestDto.author())
                .title(bookRequestDto.title())
                .price(bookRequestDto.price())
                .build());
    }

    @Override
    public List<BookResponseDto> getAll() {
        List<Book> bookList = bookRepository.findAll();
        List<BookResponseDto> bookResponseDtoList = bookList.stream()
                .map(b -> new BookResponseDto(b.getTitle(), b.getAuthor(), b.getPrice())).toList();
        return bookResponseDtoList;
    }

    @Override
    public BookResponseDto getById(UUID uuid) {
        BookResponseDto bookResponseDto = bookRepository.findById(uuid).map(b -> new BookResponseDto(b.getTitle(), b.getAuthor(), b.getPrice())).orElseThrow();
        System.out.println(bookResponseDto);
        return bookResponseDto;
    }
}
