package com.example.bookms.dto.response;

import java.math.BigDecimal;

public record BookResponseDto(String title, String author, BigDecimal price) {
}
