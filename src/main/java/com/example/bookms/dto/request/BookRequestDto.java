package com.example.bookms.dto.request;

import java.math.BigDecimal;

public record BookRequestDto(String title, String author, BigDecimal price) {
}
